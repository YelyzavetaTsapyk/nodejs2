const config = require('../config/authConfig');
const User = require('../models/userModel');
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const saltRounds = 10;

function sendError(res, status, message) {
    res.status(status).send({
        message: message
    });
}

function sendResult(res, message, result = {}) {
    res.send({
        message: message,
        ...result
    });
}

async function encrypt(value) {
    let promise = new Promise((resolve) => {
        bcrypt.hash(value, saltRounds, async (err, hash) => {
            resolve(hash);
        });
    });
    return await promise;
}


module.exports = {
    registerUser: async (req, res) => {
        try {
            const user = new User(req.body);
            const found = await User.findOne({"username": user.username});
            if (!found) {
                user.password = bcrypt.hashSync(req.body.password, saltRounds);
                await user.save();
                sendResult(res, 'Success');
            } else {
                sendError(res, 400, 'Username already exists')
            }
        } catch (error) {
            sendError(res, 400, 'Bad request')
        }
    },
    loginUser: async (req, res) => {
        //try {
            const user = new User(req.body);
            const found = await User.findOne({"username": user.username});
            if (found) {
                const result = bcrypt.compareSync(user.password, found.password)
                if (result) {
                    const token = jwt.sign({username: user.username}, config.secret, {expiresIn: 86400});
                    sendResult(res, 'Success', {"jwt_token": token});
                } else {
                    sendError(res, 400, 'Passport is invalid');
                }
            } else {
                sendError(res, 400, `Username doesn't exist`)
            }
        /*} catch (error) {
            sendError(res, 400, 'Bad request')
        }*/
    }
}
