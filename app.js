const express = require('express');
const app = express();
app.use(express.json());

const dotenv = require('dotenv').config();

console.log(process.env.MONGODB_URI);

require('./initDB')();

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Headers", "x-access-token, Origin, Content-Type, Accept");
    next();
});

const authRouter = require("./routes/authRouter.js");

app.use("/api/auth", authRouter);
/*
app.use(function (req, res, next) {
    res.status(404).send("Not Found")
});
*/

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
    console.log('Server started on port ' + PORT + '...');
});
