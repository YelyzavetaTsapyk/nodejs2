const express = require("express");
const router = express.Router();
const UserController = require("../controllers/authController.js");


router.use("/register", UserController.registerUser);
//userRouter.use("/", UserController.getUsers);

module.exports = router;
