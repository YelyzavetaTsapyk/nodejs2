const express = require("express");
const router = express.Router();
const AuthController = require("../controllers/authController.js");


router.post("/register", AuthController.registerUser);
router.post("/login", AuthController.loginUser);
//userRouter.use("/", UserController.getUsers);

module.exports = router;
